package BackEnd;

import java.io.File;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

public class Wallet {
   /**
    * The RandomAccessFile of the wallet file
    */  
   private RandomAccessFile file;
   private FileChannel channel;

   /**
    * Creates a Wallet object
    *
    * A Wallet object interfaces with the wallet RandomAccessFile
    */
    public Wallet () throws Exception {
	this.file = new RandomAccessFile(new File("wallet.txt"), "rw");
	this.channel = file.getChannel();
	
    }

   /**
    * Gets the wallet balance. 
    *
    * @return                   The content of the wallet file as an integer
    */
    public int getBalance() throws IOException {
	this.file.seek(0);
	return Integer.parseInt(this.file.readLine());
    }

   /**
    * Sets a new balance in the wallet
    *
    * @param  newBalance          new balance to write in the wallet
    */
    public void setBalance(int newBalance) throws Exception {
	this.file.setLength(0);
	String str = new Integer(newBalance).toString()+'\n'; 
	this.file.writeBytes(str); 
    }

   /**
    * Closes the RandomAccessFile in this.file
    */
    public void close() throws Exception {
	this.file.close();
    }
    public void safeWithdraw(int valueToWithdraw) throws Exception
    {
    	//try to lock the file,
    	//do withdraw,
    	//release lock
    	
    	FileLock lock = channel.tryLock();
    	
    	int curBalance = getBalance();
    	int newBalance = curBalance - valueToWithdraw;
    	
    	if (newBalance < 0)
    		throw new Exception("Withdraw amount exceeds the balance");
    	else
    		setBalance(newBalance);
    	
    	if( lock != null ) {
            lock.release();
        }

    }

}
