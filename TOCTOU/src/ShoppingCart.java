/*
 * You have to write a frontend for a command line shopping cart. The application (called ShoppingCart.java) has to:

print the current balance of the user
print the product list and their prices
ask a product to buy
check if the amount of credits is enough, if not stop the execution.
otherwise, withdraw the price of the product from the wallet.
add the name of the product to the pocket file.
print the new balance.
exit normally.
 * 
 * 
 * $ java ShoppingCart
Your balance: 30000 credits
pen     40
car     30000
candies 1
book    100
What you want to buy?: <insert a product name, e.g. pen>
Your new balance is: 29960 credits

$ cat pocket.txt
book
pen

$ cat wallet.txt
29960
  
 * */
import java.io.IOException;
import BackEnd.Wallet;

public class ShoppingCart {
	
	private BackEnd.Wallet wallet;
	private BackEnd.Store store;
	private BackEnd.Pocket pocket;
	
	// Get Balance from wallet.txt
	public int GetBalance(){
		try {
			wallet = new BackEnd.Wallet();
			return wallet.getBalance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	//getthe product as string from hashmap
	public String GetProducts(){
		return BackEnd.Store.asString();
	}
	
	// getitem price from hashmap through key
	public int GetItemPrice(String item){
		return BackEnd.Store.products.get(item);
	}
	
	// calculate the new balance
	public int NewBalance(String item){
		return GetBalance() - GetItemPrice(item);
	}
	
	
	public void SetNewBalance(int newbalance){
		try {
			wallet = new BackEnd.Wallet();
			wallet.setBalance(newbalance);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	// add product to pocket
	public void AddProduct(String item){
		try {
			pocket = new BackEnd.Pocket();
			pocket.addProduct(item);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Bug fix
	public void Purchase(int amount) throws Exception{
		
		wallet.safeWithdraw(amount);
	}
}
