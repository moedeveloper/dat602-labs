import java.io.BufferedReader;
import java.io.InputStreamReader;

import BackEnd.Pocket;

public class TocTouRun {

	public static void main(String[] args) throws Exception {
		
		ShoppingCart  sc = new ShoppingCart(); // dep injec instead
		
		//print the current balance of the user
		System.out.format("Your Balance is: %d\n", sc.GetBalance());
		//print the product list and their prices
		System.out.println(BackEnd.Store.asString());
		
		//ask a product to buy
		System.out.print("What you want to buy?: <insert a product name, e.g. pen>:");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	
		String inputString = br.readLine();

		//check if the amount of credits is enough, if not stop the execution.
		if(sc.GetBalance() >= sc.GetItemPrice(inputString)){
			//otherwise, withdraw the price of the product from the wallet.
			
			//Following commented code is vulnerable to race condition.
			//we fixed it by adding a purchase method to shoppingcart which calls 
			//safewithdraw on wallet.
			
			//int walletBalance = sc.NewBalance(inputString);
			//set the price in the wallet.txt
			//sc.SetNewBalance(walletBalance);
			
			//add the name of the product to the pocket file.
			//sc.AddProduct(inputString);
			//print the new balance.
			//System.out.format("Your new balance is:%d credits ", walletBalance);
			try{
				sc.Purchase(sc.GetItemPrice(inputString));
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
			}
			
			System.out.format("Balance In: %d\n", sc.GetBalance());
		}
		else{
			System.out.println("Cost exceeds the balance");
			System.exit(0);
		}	
		//exit normally.	
		System.exit(0);
	}

}
